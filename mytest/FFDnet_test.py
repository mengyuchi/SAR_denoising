import os, glob
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from glob import glob
from skimage.metrics import structural_similarity as ssim
import cv2

path_to_Train = r'C:\Users\mengy\Documents\DATA\Sentinel\data_npy'
path_to_Test = r'C:\Users\mengy\Documents\DATA\Sentinel\data_npy'
files_train = os.listdir(path_to_Train)
files_test = os.listdir(path_to_Test)

train_list=[]
test_list=[]

for file in files_train:
  if '.npy' in file:
    train_list.append(np.load(path_to_Train+str("\\")+file))

for file in files_test:
  if '.npy' in file:
    test_list.append(np.load(path_to_Test+str("/")+file))

print(len(train_list))

img_size=128

X_train=np.zeros((1,img_size,img_size))



for train_image in train_list:
  test = train_image.shape[0]
 
  for i in range(int(train_image.shape[0]/img_size)):
    for j in range(int(train_image.shape[1]/img_size)):
      im_to_add=np.expand_dims(train_image[i*img_size:(i+1)*img_size,j*img_size:(j+1)*img_size],axis=0)
      X_train=np.concatenate((X_train,im_to_add),axis=0)

X_train=X_train[1:,:,:]

print("Shape of the training set is: ",X_train.shape)