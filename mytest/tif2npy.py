import rasterio
import numpy as np
import os
 

src_folder = r'C:\Users\mengy\Documents\DATA\Sentinel\clip'
dest_folder = r'C:\Users\mengy\Documents\DATA\Sentinel\data_npy'
 
src_folder_names = os.listdir(src_folder)
    # for name in src_folder_names:
    #     dest_folder_name = os.path.join(dest_folder, name)
    #     if not os.path.exists(dest_folder_name):
    #         os.mkdir(dest_folder_name)
    # print("Folder name copied successfully!")
 
for name in src_folder_names:
    full_name = os.path.join(src_folder, name)
    file_names = os.listdir(src_folder)
    # for file_name in file_names:
    with rasterio.open(os.path.join(src_folder, name)) as src:
        img_data = src.read()
        basename, ext = os.path.splitext(name)
        np.save(os.path.join(dest_folder, basename+'.npy'), img_data[0,2000:2128,3000:3128])
        
        print('done!')